# README #


### What is this repository for? ###

* BlogBundle is a bundle to add a a blog to a project.

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before make a upload or edit a file.

##Add it to AppKernel.php.##
new \Greetik\BlogBundle\BlogBundle()


##In the config.yml you can add your own service##
blog:
    permsservice: app.blog
    interface: AppBundle:Post
