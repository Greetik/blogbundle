<?php

namespace Greetik\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\BlogBundle\Entity\Post;
use Greetik\BlogBundle\Form\Type\PostType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Render all the posts of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {

        return $this->render($this->getParameter('blog.interface').':index.html.twig', array(
                    'data' => $this->get($this->getParameter('blog.permsservice'))->getAllPostsByProject($idproject),
                    'idproject' => $idproject,
                    'insertAllow' => true
        ));
    }

    /**
     * View an individual post, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id) {
        $post = $this->get($this->getParameter('blog.permsservice'))->getPost($id);
        $editForm = $this->createForm(PostType::class, $this->get('blog.tools')->getPostObject($id), array('_tags'=>$post['tags']));
        return $this->render($this->getParameter('blog.interface').':view.html.twig', array(
                    'option'=>'',
                    'item' => $post,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get($this->getParameter('blog.permsservice'))->getPostPerm($post, 'modify'),
                    'tags'=>$this->get($this->getParameter('blog.permsservice'))->getDifferentTags($post['project'], true)
        ));
    }

    /**
     * Get the data of a new Post by Post and persis it
     * 
     * @param Post $item is received by Post Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $post = new Post();
        
        $newForm = $this->createForm(PostType::class, $post);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try{
                    $post->setTags(explode(',', $newForm->get('tags')->getData()));
                    $this->get($this->getParameter('blog.permsservice'))->insertPost($post, $idproject, $this->getUser()->getId());
                    return $this->redirect($this->generateUrl('blog_listposts', array('idproject' => $idproject)));
                }catch(\Exception $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }else{
                $this->addFlash('error', (string) $newForm->getErrors(true, true));
            }
        }
        
        return $this
                        ->render($this->getParameter('blog.interface').':insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView(),
                    'tags'=>$this->get($this->getParameter('blog.permsservice'))->getDifferentTags($idproject, true)));
    }

    /**
     * Edit the data of an post
     * 
     * @param int $id is received by Get Request
     * @param Post $item is received by Post Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $post = $this->get('blog.tools')->getPostObject($id);
        $oldtags = $post->getTags();
        
        $editForm = $this->createForm(PostType::class, $post);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            
            if ($editForm->isValid()) {
                try{
                    $post->setTags(explode(",",$editForm->get('tags')->getData()));
                    $this->get($this->getParameter('blog.permsservice'))->modifyPost($post, $oldtags);
                }catch(\Exception $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('blog_viewpost', array('id' => $id)));
    }

    public function deleteAction(Request $request, $id) {
        
        $post = $this->get('blog.tools')->getPostObject($id);
        if ($request->getMethod() == "POST") {
            $this->get($this->getParameter('blog.permsservice'))->deletePost($post);
            return $this->redirect($this->generateUrl('blog_listposts', array('idproject' => $post->getProject())));
        }
        return $this->redirect($this->generateUrl('blog_listposts'));
    }

}
