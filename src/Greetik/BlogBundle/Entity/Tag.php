<?php

namespace Greetik\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table(name="tag", indexes={
 *      @ORM\Index(name="name", columns={"name"}),  @ORM\Index(name="repetitions", columns={"repetitions"}),  @ORM\Index(name="project", columns={"project"}),  @ORM\Index(name="namreppro", columns={"name", "repetitions", "project"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\BlogBundle\Repository\TagRepository")
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=511)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="repetitions", type="integer")
     */
    private $repetitions;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set repetitions
     *
     * @param integer $repetitions
     *
     * @return Tag
     */
    public function setRepetitions($repetitions)
    {
        $this->repetitions = $repetitions;

        return $this;
    }

    /**
     * Get repetitions
     *
     * @return integer
     */
    public function getRepetitions()
    {
        return $this->repetitions;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Tag
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }
}
