<?php

namespace Greetik\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostType
 *
 * @author Paco
 */
class PostType extends AbstractType {

    

    public function buildForm(FormBuilderInterface $builder, array $options) {

        if (!empty($options['_tags']))
            $this->tags = implode(',', $options['_tags']);
        else
            $this->tags = '';

        if (count($options['projects'])>0) {
            $builder
                    ->add('project', 'choice', array(
                        'choices' => $options['projects'],
                        'required' => true,
                        'expanded' => false,
                        'multiple' => false
            ));
        }
        $builder
                ->add('title', TextType::class)
                ->add('body', TextareaType::class, array('required' => false))
                ->add('tags', TextType::class, array('required' => false, 'mapped' => false, 'data' => $this->tags))
                ->add('publishdate', DatetimeType::class, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm',))
                ->add('enddate', DatetimeType::class, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm', 'required' => false))
                ->add('metatitle', TextType::class, array('required' => false))
                ->add('metadescription', TextType::class, array('required' => false))
                ->add('extra', TextType::class, array('required' => false));
    }

    public function getName() {
        return 'Post';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\BlogBundle\Entity\Post',
            '_tags' => null,
            'projects' => array()
        ));
    }

}
